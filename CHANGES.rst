==============
Qinbin Changes
==============

v1.3.0
======

Release TBD.

Added
-----

- This changelog.
- An update check which runs at application launch
  and checks the local version
  against Bitbucket tags.
  If a newer version is found,
  the user is prompted to upgrade.
- Steps toward PyPI distribution.

Fixed
-----

- Report worker should bail
  if connecting to datasources fails.
- Tracebacks should go to stderr, not stdout.

v1.2.0
======

Released 2019-03-22.

Added
-----

- Prompt to overwrite the output file.
  Don't just do it silently: that's not nice.

Fixed
-----

- Report was only opening upon completion on Windows.
  A fix implemented to make Windows work –
  running the process in a shell –
  was unnecessary on other platforms,
  and in fact broke the feature.
- Better handling of non-string values
  returned by the database.
  Convert byte arrays to strings,
  and make sure values are strings
  before treating them as such
  (e.g., regex evaluation).
- Correctly load the last-used datasource
  from the configuration file.

v1.1.0
======

Released 2018-10-17.

Added
-----

- Open the report on completion.
- Added an application icon.
- Generate a Windows installer.

v1.0.0
======

Released 2018-10-15.

Added
-----

- Everything! This was the initial release.
