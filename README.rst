.. This document is written
   using Semantic Linefeeds.
   See http://rhodesmill.org/brandon/2012/one-sentence-per-line/
   for an explanation
   of why linebreaks are
   the way they are.)

======
Qinbin
======

Introduction
============

Qinbin is a tool
for generating Excel workbooks
(XLSX files)
from the results
of arbitrary database queries.

The project is named after a combination of “Tintin”,
the comic book reporter,
and the initials of QuickBooks,
the datasource for which
it was originally conceived.

Installing
==========

From binaries
-------------

Pre-built Windows binaries (.exe)
and installers (.msi)
can be downloaded `from Bitbucket`_.

.. _from Bitbucket: https://bitbucket.org/rbr/qinbin/downloads/

From source
-----------

To install from source:

.. code-block:: shell

    $ git clone https://bitbucket.org/rbr/qinbin
    $ cd qinbin
    $ pip3 install -e '.[MySQL,PostgreSQL,SQLAnywhere]'

You don't *need* the extra packages,
but at least one or two of them
are probably useful.
And if you're building the application into
a single-file package,
you'll want them available
so that there are some drivers included
in that executable.

To build the project as
a standalone application:

.. code-block:: shell

    $ # Make sure you have PyInstaller available.
    $ pip3 install pyinstaller
    $ make linux

Or on Windows,
in a MinGW environment:

.. code-block:: shell

    $ make windows

That will create
a single-file application
in ``dist``.

Running
=======

To launch the application
from the source code directory,

.. code-block:: shell

    $ python3 -m qinbin

If you've ``pip3 install``'d it,
you can run that from anywhere.

Or, if you've built the project
as a standalone application,

.. code-block:: shell

    $ ./dist/qinbin

Configuring
===========

There must be a ``config.json`` file
in your working directory.

.. code-block:: json

    {
        "datasources": [
            {
                "name": "Test Datasource",
                "module": "mysql.connector",
                "parameters": {
                    "host": "127.0.0.1",
                    "user": "username",
                    "password": "password",
                    "database": "sakila"
                }
            }
        ],
        "report_path": "/path/to/a/report.sql",
        "output_path": "/path/to/a/report - 2018-10-15.xlsx",
        "datasource": "Test Datasource"
    }

The only required key
is ``datasources``.
It must be an array of datasource definitions.
A datasource definition consists of

- a human-readable ``name``
- the name of the Python ``module`` to import
- any ``parameters`` required for the connection

At startup,
Qinbin imports the specified module by name,
then calls the ``connect`` function
in that module,
passing the specified parameters.
It expects the return value
to be a `PEP 249`_-compliant database connection.

.. _PEP 249: https://www.python.org/dev/peps/pep-0249/

The ``report_path`` is whatever the report path was
when the application was last closed.
Similarly, the ``output_path`` and ``datasource``
are the output path and datasource selected
when the application was last closed.
