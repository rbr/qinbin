# -*- mode: python -*-

import os

spec_path = os.path.abspath(SPECPATH)

exec(open(os.path.join(spec_path, 'qinbin/version.py')).read())

block_cipher = None


a = Analysis(['qinbin/__main__.py'],
             pathex=[spec_path],
             binaries=[('qinbin/icon.png', 'qinbin')],
             datas=[],
             hiddenimports=['mysql', 'mysql.connector', 'psycopg2', 'sqlanydb'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='qinbin-{}'.format(__version__),
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False , icon='icon.ico')
