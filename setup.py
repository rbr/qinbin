#! /usr/bin/env python3

import os.path
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))

exec(open(os.path.join(here, 'qinbin/version.py')).read())

with open(os.path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name=APP_NAME,
    version=__version__,

    description=__doc__.strip().split('\n\n', 1)[0],
    long_description=long_description,

    url='https://bitbucket.org/rbr/qinbin',

    author=__author__,
    author_email=__contact__,

    license=__license__,

    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Topic :: Database :: Front-Ends',
        'Topic :: Office/Business :: Financial :: Spreadsheet',
    ],

    packages=['qinbin'],
    package_data={'qinbin': ['*.png']},

    scripts=['scripts/qinbin'],

    install_requires=['appdirs', 'packaging', 'xlsxwriter'],

    extras_require={
        'MySQL': ['mysql-connector-python'],
        'PostgreSQL': ['psycopg2'],
        'SQLAnywhere': ['sqlanydb']
    },
)
