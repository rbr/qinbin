SOURCE := qinbin/__init__.py \
          qinbin/__main__.py \
          qinbin/gui.py \
          qinbin/report.py \
          qinbin/version.py \
          qinbin/icon.png

VERSION = $(shell grep __version__ qinbin/version.py \
    | grep -o "'.\+'" \
    | sed -e "s/^'//" -e "s/'$$//")

default:
	@echo "Usage: make linux|windows"

linux: dist/qinbin-$(VERSION)
dist/qinbin-$(VERSION): $(SOURCE)
	pyinstaller qinbin.spec

windows: dist/qinbin-$(VERSION).exe installer
dist/qinbin-$(VERSION).exe: $(SOURCE)
	pyinstaller qinbin.spec

installer: dist/qinbin-$(VERSION).msi

dist/qinbin-$(VERSION).msi: build/qinbin.wixobj dist/qinbin-$(VERSION).exe
	light -out $@ $<

# Dependency on version.py so the wixobj gets regenerated when the version
# number changes.
build/qinbin.wixobj: qinbin.wxs qinbin/version.py
	candle -out $@ -arch x64 -dQinbin.Version=$(VERSION) $<

.PHONY: clean
clean:
	rm -Rf build dist Qinbin.egg-info qinbin.msi
