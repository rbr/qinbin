#! /usr/bin/env python3

import os
import queue
import datetime
import subprocess
import sys
import tkinter
import tkinter.filedialog
import tkinter.messagebox
import tkinter.ttk
import webbrowser

from . import report, version

# How frequently should we update the job status label?
UPDATE_FRAMERATE = int(1000 / 15)

_DOWNLOAD_URL = 'https://bitbucket.org/rbr/qinbin/downloads/'

_OS_OPEN_PARAMS = {}
if sys.platform.startswith('win32'):
    _OS_OPEN = 'start'
    _OS_OPEN_PARAMS['shell'] = True
elif sys.platform.startswith('darwin'):
    _OS_OPEN = 'open'
else:
    _OS_OPEN = 'xdg-open'

class QinbinGui:
    def __init__(self, config, worker):
        self._config = config
        self._worker = worker
        self._start_time = None

        self._root = tkinter.Tk()
        self._root.title('{} v{}'.format(version.APP_NAME, version.__version__))
        self._root.resizable(False, False)

        icon_path = os.path.join(os.path.dirname(__file__), 'icon.png')
        icon = tkinter.PhotoImage(file=icon_path)
        self._root.iconphoto(True, icon)

        self._datasource_label = tkinter.Label(self._root, text='Datasource:')
        self._datasource_label.grid(row=1, column=1, sticky=tkinter.E)

        self._datasource = tkinter.StringVar(self._root)
        self._datasource.set(self._config['datasource'])
        self._datasource.trace('w', lambda name, index, operation: self._config.__setitem__('datasource', self._datasource.get()))

        self._datasource_listbox = tkinter.OptionMenu(self._root, self._datasource, *[datasource['name'] for datasource in self._config['datasources']])
        self._datasource_listbox.grid(row=1, column=2, columnspan=2, sticky=tkinter.W + tkinter.E)

        self._report_label = tkinter.Label(self._root, text='Report:')
        self._report_label.grid(row=2, column=1, sticky=tkinter.E)

        self._report_path = tkinter.StringVar(self._root)
        self._set_report_path(self._config['report_path'])
        self._report_path.trace('w', lambda name, index, operation: self._config.__setitem__('report_path', self._report_path.get()))

        self._report_path_entry = tkinter.Entry(self._root, textvariable=self._report_path, width=60)
        self._report_path_entry.grid(row=2, column=2, sticky=tkinter.W + tkinter.E)

        self._report_browse = tkinter.Button(self._root, text='Browse...', command=self._prompt_report_path)
        self._report_browse.grid(row=2, column=3)

        self._output_label = tkinter.Label(self._root, text='Output:')
        self._output_label.grid(row=3, column=1, sticky=tkinter.E)

        self._output_path = tkinter.StringVar(self._root)
        self._output_path.trace('w', lambda name, index, operation: self._config.__setitem__('output_path', self._output_path.get()))

        self._output_path_entry = tkinter.Entry(self._root, textvariable=self._output_path, width=60)
        self._output_path_entry.grid(row=3, column=2, sticky=tkinter.W + tkinter.E)

        self._output_browse = tkinter.Button(self._root, text='Browse...', command=self._prompt_output_path)
        self._output_browse.grid(row=3, column=3)

        self._run_button = tkinter.Button(self._root, text='Run Report', state='disabled', command=self._run_report, width=12, height=3)
        self._run_button.grid(row=4, column=1, columnspan=3)

        self._progress = tkinter.IntVar(self._root)
        self._progress.set(0)

        self._status = tkinter.StringVar(self._root)
        self._status_label = tkinter.Label(self._root, textvariable=self._status)
        self._status_label.grid(row=6, column=1, columnspan=2, sticky=tkinter.W)

        self._timer = tkinter.StringVar(self._root)
        self._timer_label = tkinter.Label(self._root, textvariable=self._timer)
        self._timer_label.grid(row=6, column=3, sticky=tkinter.E)

        self._set_output_path_from_report_path()
        self._update_status()

        new_version = version.update_check()
        if new_version is not None:
            if tkinter.messagebox.askyesno(version.APP_NAME, "A new version of {} is available, v{}. Do you want to download it now?".format(version.APP_NAME, new_version)):
                webbrowser.open(_DOWNLOAD_URL)

    def mainloop(self):
        self._root.mainloop()

    def _set_report_path(self, path):
        self._report_path.set(os.path.abspath(path))

    def _prompt_report_path(self):
        path = tkinter.filedialog.askopenfilename(initialdir=os.path.dirname(self._report_path.get()),
                                                  filetypes=(('SQL files', '*.sql'),))
        if len(path) == 0:
            return

        self._set_report_path(path)
        self._set_output_path_from_report_path()

    def _set_output_path(self, path):
        self._output_path.set(os.path.abspath(path))

    def _set_output_path_from_report_path(self):
        report_path = self._report_path.get()

        now = datetime.datetime.now()
        output_path = '{} - {}.xlsx'.format(report_path.rsplit('.', 1)[0],
                                            now.strftime('%F'))
        self._set_output_path(output_path)

    def _prompt_output_path(self):
        output_path = self._output_path.get()
        if output_path != '':
            initial_dir = os.path.dirname(output_path)
        elif 'report_path' in self._config:
            initial_dir = os.path.dirname(self._config['report_path'])
        else:
            initial_dir = None

        path = tkinter.filedialog.asksaveasfilename(initialdir=initial_dir,
                                                    filetypes=(('Office Open XML Spreadsheet', '*.xlsx'),))
        if path == '':
            return

        self._set_output_path(output_path)

    def _run_report(self):
        report_path = self._report_path.get()
        output_path = self._output_path.get()
        conn = self._datasource.get()

        if report_path == '':
            tkinter.messagebox.showerror(version.APP_NAME, "You didn't specify a report file. Pick one and try again.")
            return
        if not os.path.isfile(report_path):
            tkinter.messagebox.showerror(version.APP_NAME, "No report file exists at the given path. Pick a real file and try again.")
            return
        if not os.access(report_path, os.R_OK):
            tkinter.messagebox.showerror(version.APP_NAME, "That report file isn't readable. Check access permissions and try again.")
            return

        if output_path == '':
            tkinter.messagebox.showerror(version.APP_NAME, "You didn't specify an output file. Pick one and try again.")
            return
        if ((os.path.isfile(output_path) and not os.access(output_path, os.W_OK))
            or not os.access(os.path.dirname(output_path), os.W_OK)):
            tkinter.messagebox.showerror(version.APP_NAME, "That output file isn't writable. Check access permissions and try again.")
            return
        if (os.access(output_path, os.R_OK)):
            if not tkinter.messagebox.askyesno(version.APP_NAME, "That output file already exists. Are you sure you want to overwrite it?"):
                return

        self._run_button['state'] = 'disabled'
        self._worker.jobs.put(report.ReportJob(report_path, output_path, conn))
        self._start_time = datetime.datetime.now()

    def _update_status(self):
        if self._start_time is not None:
            elapsed = datetime.datetime.now() - self._start_time
            elapsed_formatted = str(elapsed)[:-5]
            self._timer.set(elapsed_formatted)

        try:
            message = self._worker.messages.get_nowait()

            if message.type == 'status':
                self._status.set(message.value)
            elif message.type == 'error':
                tkinter.messagebox.showerror(version.APP_NAME, 'I encountered an error:\n\n{}'.format(message.value))
            elif message.type == 'ready' or message.type == 'done':
                self._start_time = None
                self._run_button['state'] = 'normal'

                if message.type == 'done':
                    job = message.value
                    subprocess.Popen([_OS_OPEN, job.output_path], **_OS_OPEN_PARAMS)

                    tkinter.messagebox.showinfo(version.APP_NAME, 'Report finished in {}s.'.format(elapsed_formatted))
        except queue.Empty:
            pass

        self._root.after(UPDATE_FRAMERATE, self._update_status)
