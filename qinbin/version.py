#! /usr/bin/env python3

"""
Qinbin is a tool for generating Excel workbooks (XLSX files) from the results
of arbitrary database queries.
"""

__version__ = '1.2.0'

__author__ = 'Samuel Coleman'
__contact__ = 'samuel.coleman@rbr-global.com'

__copyright__ = 'Copyright (c) 2018, 2019 RBR Ltd.'
__license__ = 'APLv2'

APP_NAME = 'Qinbin'

_TAGS_URL = 'https://bitbucket.org/api/2.0/repositories/rbr/qinbin/refs/tags?pagelen=1&sort=-name'
_TAGS_TIMEOUT = 1.0

def update_check():
    # Lots of things import the version, including setup.py. I don't want all
    # these imports to run unless they're actually necessary.
    import json
    import packaging.version
    import sys
    import traceback
    import urllib.request

    current_version = packaging.version.parse(__version__)

    try:
        tags_request = urllib.request.urlopen(_TAGS_URL, timeout=_TAGS_TIMEOUT)
        raw_tags = tags_request.read()
        tags = json.loads(raw_tags)
        for tag in tags['values']:
            tag_version = packaging.version.parse(tag['name'])
            if tag_version > current_version:
                return str(tag_version)
    except:
        # Version update check isn't mission-critical. Log it in a quiet way
        # (not Tk)and move on.
        print('Failed to check for updates!', file=sys.stderr)
        traceback.print_exc(file=sys.stderr)

    return None
