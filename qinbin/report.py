#! /usr/bin/env python3

from collections import namedtuple
import importlib
import multiprocessing
import os
import re
import sys
import traceback
import xlsxwriter
import xlsxwriter.utility

ReportJob = namedtuple('ReportJob', ['report_path', 'output_path', 'conn'])
ReportMessage = namedtuple('ReportMessage', ['type', 'value'])

class ReportWorker(multiprocessing.Process):
    def __init__(self, config):
        super().__init__()
        self._config = config

        self.jobs = multiprocessing.Queue()
        self.messages = multiprocessing.Queue()

    def run(self):
        self._report_message('status', 'Connecting datasources...')
        try:
            self._connect_datasources()
        except Exception as e:
            traceback.print_exc(file=sys.stderr)
            self._report_message('error', e)
            return

        self._report_message('ready', None)
        self._report_message('status', 'Ready.')
        while True:
            job = self.jobs.get()

            try:
                self._run_report(job)
            except Exception as e:
                traceback.print_exc(file=sys.stderr)
                self._report_message('error', e)

            self._report_message('done', job)

    def _connect_datasources(self):
        for datasource in self._config['datasources']:
            module = importlib.import_module(datasource['module'])
            datasource['connection'] = module.connect(**datasource['parameters'])

    def _disconnect_datasources(self):
        for datasource in self._config['datasources']:
            if 'connection' in datasource:
                datasource['connection'].close()

    def _report_message(self, type, value):
        message = ReportMessage(type, value)
        self.messages.put(message)

    def _run_report(self, job):
        self._report_message('status', 'Starting...')

        conn = None
        for datasource in self._config['datasources']:
            if datasource['name'] == job.conn:
                conn = datasource['connection']
                break

        if conn is None:
            raise Exception("The selected datasource isn't connected. This should have happened at launch, but it didn't. Try restarting the program.")

        report_name = os.path.basename(job.report_path).rsplit('.', 1)[0]

        with open(job.report_path, 'r') as report_file:
            report_query = report_file.read()

        workbook = xlsxwriter.Workbook(job.output_path)
        worksheet = workbook.add_worksheet(report_name)

        curr = conn.cursor()

        try:
            self._report_message('status', 'Beginning query execution...')
            curr.execute(report_query)

            # Skip a row for the header, which will be filled in at the end.
            row_index = 1
            cell_index = 0

            self._report_message('status', 'Retrieving data to worksheet...')
            while True:
                row = curr.fetchone()
                if row is None:
                    break

                cell_index = 0
                for cell in row:
                    if cell is None:
                        pass

                    if isinstance(cell, bytearray):
                        cell = cell.decode()

                    if isinstance(cell, str) and re.match('^0[0-9]+$', cell):
                        worksheet.write_string(row_index, cell_index, cell)
                    elif isinstance(cell, int) \
                            or isinstance(cell, float) \
                            or (isinstance(cell, str) \
                                and re.match('^-?([0-9]+(\.[0-9]+)?|\.[0-9]+)$', cell)):
                        worksheet.write_number(row_index, cell_index, float(cell))
                    else:
                        worksheet.write(row_index, cell_index, cell)
                    cell_index += 1
                row_index += 1

            self._report_message('status', 'Writing worksheet header...')
            headers = [description[0] for description in curr.description]
            worksheet.add_table('A1:' + xlsxwriter.utility.xl_rowcol_to_cell(row_index - 1, cell_index - 1),
                                {'columns': [{'header': header} for header in headers]})

            self._report_message('status', 'Done.')
        finally:
            curr.close()
            workbook.close()
