#! /usr/bin/env python3

import appdirs
import json
import os.path
import sys
import tkinter.messagebox
import traceback

from . import gui, report, version

def load_config():
    config_paths = [
        'config.json',
        os.path.join(appdirs.user_config_dir(version.APP_NAME, 'RBR'), 'config.json')
    ]

    config_path = None
    for possible_config_path in config_paths:
        if os.path.isfile(possible_config_path):
            config_path = possible_config_path
            break

    if config_path is None:
        raise Exception("Couldn't find configuration. I tried:\n{}".format(''.join(['\n - {}'.format(path) for path in config_paths])))

    print('Loading config from {}.'.format(config_path))

    with open(config_path, 'r') as config_file:
        config_json = config_file.read()

    config = json.loads(config_json)
    config['config_path'] = config_path

    if 'datasources' not in config or len(config['datasources']) == 0:
        raise Exception('I found configuration at {}, but it contains no datasource definitions.'.format(config_path))

    datasource_names = [datasource['name'] for datasource in config['datasources']]
    if 'datasource' not in config or config['datasource'] not in datasource_names:
        config['datasource'] = config['datasources'][0]['name']

    if 'report_path' not in config:
        config['report_path'] = ''

    return config

def save_config(config):
    cleaned_config = dict(config)
    config_path = cleaned_config.pop('config_path', None)
    for datasource in cleaned_config['datasources']:
        if 'connection' in datasource:
            del datasource['connection']

    print('Saving config to {}.'.format(config_path))

    with open(config_path, 'w') as config_file:
        config_file.write(json.dumps(cleaned_config))

def main():
    try:
        config = load_config()
    except Exception as e:
        print('Failed to load configuration!', file=sys.stderr)
        traceback.print_exc(file=sys.stderr)
        tkinter.messagebox.showerror(version.APP_NAME, 'Failed to load configuration:\n\n{}'.format(e))
        return 1

    worker = report.ReportWorker(config)
    worker.start()

    try:
        gui.QinbinGui(config, worker).mainloop()
    except KeyboardInterrupt:
        pass

    worker.terminate()

    save_config(config)

    return 0

if __name__ == '__main__':
    sys.exit(main())
